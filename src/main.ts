import './style.css'
import P5 from "p5";
import MovingCircleSketch from "./MovingCircleSketch"

const sketch = MovingCircleSketch.toP5Sketch();

new P5(sketch);
