import p5 from "p5";

type Vector2 = {x: number, y : number}

class Circle {
    constructor(
        private pos: Vector2, 
        private moveVector: Vector2, 
        private radius: number, 
        private color: string
    ) {}

    public draw(p: p5, amplitude: number) {
        p.fill(this.color);
        const x = this.pos.x + amplitude * this.moveVector.x;
        const y = this.pos.y + amplitude * this.moveVector.y;
        p.ellipse(x,y, this.radius);
    }
}

export default Circle;