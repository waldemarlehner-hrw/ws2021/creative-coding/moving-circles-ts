import p5 from "p5";
import Circle from "./Circle";

class MovingCircleSketch {
	private amplitetude = 20;
	private smallCircleRadius = 5;
	private bigCircleRadius = 10;
	private count = {x: 20, y: 30}
	private bigCircleColor = "#fcba03";
	private smallCircleColor = "#FFFFFF";
	
	private canvas?: p5.Renderer;
	private circles: Circle[] = [];

	private get screenSizeX(){
		return this.bigCircleRadius * this.count.x * 2
	}

	private get screenSizeY(){
		return this.bigCircleRadius * this.count.y * 2
	}

	constructor(private p: p5){
		p.setup = () => this.setup();
		p.draw = () => this.draw();
	}

	private setup() {
		this.p.noStroke();
		this.createCircles(this.bigCircleRadius, this.bigCircleColor);
		this.createCircles(this.smallCircleRadius, this.smallCircleColor);

		this.canvas = this.p.createCanvas(this.screenSizeX, this.screenSizeY);
		this.canvas.parent("app");
	}

	private createCircles(radius: number, color: string) {
		for(let x = 0; x < this.count.x; x++) {
			for(let y = 0; y < this.count.y; y++) {
				const pos = {x: (2*x+1)*this.bigCircleRadius, y: (2*y+1)*this.bigCircleRadius};
				const vector = {x: 2 * Math.random() - 1, y: 2 * Math.random() - 1};
				this.circles.push(new Circle(pos, vector, 2* radius, color));
			}
		}
	}

	private draw() {
		this.p.background("#FFFFFF");
		const mouse = {x: MovingCircleSketch.clamp(this.p.mouseX, 0, this.screenSizeX), y: MovingCircleSketch.clamp(this.p.mouseY, 0, this.screenSizeY)};
		const amplitude = ( mouse.x + mouse.y ) / (this.screenSizeX + this.screenSizeY);
		for(const circle of this.circles) {
			circle.draw(this.p, amplitude * this.amplitetude);
		}
	}
	
	public static toP5Sketch(): (p5: p5) => void {
		return (p5: p5) => new MovingCircleSketch(p5);
	}

	private static clamp(val: number, lower: number, upper: number) {
		if(val < lower)
			return lower;
		if(val > upper)
			return upper;
		return val;
	}
}

export default MovingCircleSketch;
